import time

import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_variable


BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'

schema = {
    'BITBUCKET_USERNAME': {'required': True, 'type': 'string'},
    'BITBUCKET_PASSWORD': {'required': True, 'type': 'string'},
    'ACCOUNT': {'nullable': True, 'required': False, 'type': 'string'},
    'REPOSITORY': {'required': False, 'type': 'string'}
}


class BitbucketReport(Pipe):

    def _get_repositories(self, username, password, account):
        pagelen = 100
        page = 1
        repositories = []
        while True:
            params = {
                'sort': "-created_on",
                'page': page,
                'pagelen': 100
            }
            repository_request = requests.get(f"{BITBUCKET_API_BASE_URL}/repositories/{account}/", params=params, auth=HTTPBasicAuth(username, password))
            repository_request.raise_for_status()
            repositories.extend((repository_request.json())['values'])
            if len(repositories) == repository_request.json()['size']:
                break
            page = page + 1
        return repositories

    def _get_pipelines(self, username, password, repository_path):
        """
        Retrieves pipelines list
        :param repository_path: Repository path (ex: account/repo)
        :param username: Username
        :param password: Password
        :return: List of pipelines information in JSON format
        """
        page = 1
        pipelines = []
        while True:
            params = {
                'sort': "-created_on",
                'page': page,
                'pagelen': 100
            }
            pipelines_request = requests.get(f"{BITBUCKET_API_BASE_URL}/repositories/{repository_path}/pipelines/", params=params,
                                             auth=HTTPBasicAuth(username, password))
            pipelines_request.raise_for_status()
            pipelines.extend((pipelines_request.json())['values'])
            if pipelines_request.json()['pagelen'] != 100:
                break
            page = page + 1
        return pipelines

    def _generate_report(self, username, password, account, repository, filename):

        lines = ['repository,build_minutes_used\n']
        if repository is None:
            repositories = self._get_repositories(username, password, account)
        else:
            repositories = [
                {
                    'full_name': f"{account}/{repository}"
                }
            ]
        for repository in repositories:
            full_name = repository['full_name']
            result = self._get_pipelines(username, password, full_name)
            build_seconds_used = sum(c['build_seconds_used'] for c in result)
            # duration_in_seconds = sum(c['duration_in_seconds'] for c in result)
            lines.append(full_name + "," + time.strftime('%H:%M:%S', time.gmtime(build_seconds_used)) + "\n")

        fh = open(filename, "w")
        fh.writelines(lines)
        fh.close()

    def run(self):
        username = get_variable('BITBUCKET_USERNAME')
        password = get_variable('BITBUCKET_PASSWORD')
        account = get_variable('ACCOUNT')
        repository = get_variable('REPOSITORY')

        self._generate_report(username, password, account, repository, "output.csv")


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = BitbucketReport(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
