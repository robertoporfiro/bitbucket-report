# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.6

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.5

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.4

- patch: Add warning message about new version of the pipe available.

## 0.3.3

- patch: Internal maintenance: update pipes toolkit version

## 0.3.2

- patch: Change CSV header from build_seconds_used to build_minutes_used

## 0.3.1

- patch: Add header for the report

## 0.3.0

- minor: Fix REPOSITORY and ACCOUNT parameters

## 0.2.0

- minor: Fix REPOSITORY parameter

## 0.1.0

- minor: Initial release

