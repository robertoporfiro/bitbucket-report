# Bitbucket Pipelines Pipe: Bitbucket Report

Generate a report with useful data.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/bitbucket-report:0.3.6
  variables:
    BITBUCKET_USERNAME: "<string>"
    BITBUCKET_PASSWORD: "<string>"
    # ACCOUNT: "<string>" # Optional
    # REPOSITORY: "<string>" # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable                | Usage                                                       |
| ----------------------- | ----------------------------------------------------------- |
| BITBUCKET_USERNAME (*)  | Bitbucket user. |
| BITBUCKET_PASSWORD (*)  | [Bitbucket app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) of the user that will trigger the pipeline. |
| ACCOUNT                 | The name of the team or personal account to generate the report from. Default `$BITBUCKET_REPO_OWNER` |
| REPOSITORY              | The name of the repository to generate the report.  |
| DEBUG                   | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

## Examples

Basic example: Generate the report from the current repository.

```yaml
script:
  - pipe: atlassian/bitbucket-report:0.3.6
    variables:
      BITBUCKET_USERNAME: ${BITBUCKET_USERNAME}
      BITBUCKET_PASSWORD: ${BITBUCKET_PASSWORD}
```

Advanced example: Generate the report from a different repository. The Bitbucket user needs to have access to the specific account and repository.

```yaml
script:
  - pipe: atlassian/bitbucket-report:0.3.6
    variables:
      BITBUCKET_USERNAME: ${BITBUCKET_USERNAME}
      BITBUCKET_PASSWORD: ${BITBUCKET_PASSWORD}
      ACCOUNT: "my-account" 
      REPOSITORY: "my-specific-repo"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,report
